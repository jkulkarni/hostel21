package hostel21;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;

public class MainTest {

	@Test
	public void XmlLoad() throws FileNotFoundException {

		Application app = new Application(new String[] { "admin", "-load",
				getClass().getResource("/hostel.xml").getPath() });

		app.run();
		assertEquals(app.getSession().getRoot().getHostel().get(0).getName(),
				"Hostel 21 - Romantic");

	}
	

	@Test
	public void testSearchAll() throws FileNotFoundException {
		XmlLoad();
		Application app = new Application(new String[] {"search"});
		app.run();
	}


	@Test
	public void testSearch() throws FileNotFoundException {
		XmlLoad();
		Application app = new Application(new String[] {"search", "--city", "San Francisco"});
		app.run();

	}
	
	@Test
	public void testDate() throws FileNotFoundException {
		XmlLoad();
		Application app = new Application(new String[] {"search","--city","San Francisco","--start_date","20140701","--end_date","20140702"});
		app.run();

	}
	
	@Test
	public void testbadDate() throws FileNotFoundException {
		XmlLoad();
		Application app = new Application(new String[] {"search","--city","SanFrancisco","--start_date","20140701","--end_date","20140702"});
		app.run();

	}
	
	
	public void testBed() throws FileNotFoundException {
		XmlLoad();
		Application app = new Application(new String[] {"search","--city","San Francisco","--start_date","20140701","--end_date 20140702","--beds 1"});
		app.run();

	}
	

}
