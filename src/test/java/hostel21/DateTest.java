package hostel21;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class DateTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws ParseException {
		assertTrue(DateFunctions.inRange("20121202", "20121201", "20121203"));
		assertTrue(DateFunctions.inRange("20121201", "20121201", "20121201"));
		assertTrue(DateFunctions.toEpoch("20121203") - DateFunctions.toEpoch("20121202") == 86400000);
		assertTrue(DateFunctions.getDaysBetween("20121201", "20121203")==2);
		assertTrue(DateFunctions.getDaysBetween("20121201", "20121202")==1);
	}

}
