package hostel21;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

public class XmlTest {

	@Test
	public void test() throws FileNotFoundException {
		Root root = XmlFunctions.importXml(getClass()
				.getResource("/hostel.xml").getPath());
		System.out.println(root.hostel.get(0).toString());
		assertEquals(root.getHostel().get(0).getName(), "Hostel 21 - Romantic");
	}

}
