package hostel21;

import java.io.Serializable;
import java.util.ArrayList;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("hostel")
public class Hostel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7605581505750537725L;
	@XStreamAlias("name")
	private String name;
	private Address address;
	private Contact contact;
	private Restriction restrictions;
	@XStreamImplicit(itemFieldName = "availability")
	private ArrayList<Availability> availability;

	public Address getAddress() {
		return address;
	}

	public ArrayList<Availability> getAvailability() {
		return availability;
	}

	public Contact getContact() {
		return contact;
	}

	public String getName() {
		return name;
	}
	public String getInfo(){
		return name+", "+address.getCity();
	}

	public Restriction getRestrictions() {
		return restrictions;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setAvailability(ArrayList<Availability> availability) {
		this.availability = availability;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRestrictions(Restriction restrictions) {
		this.restrictions = restrictions;
	}



	public String niceString() {
		String s = "";
		s = name + "\n";
		s += "Room\tBed |\tPrice\tAvailable\tBooked by userID";
		s += "\n";
		for (Availability a : this.availability) {
			s += a.getRoom() + "\t" + a.getBed() + "\t$" + a.getPrice() + "\t"
					+ a.getDate() + "\t" + a.getBookedBy() + "\n";
		}
		return s;

	}

	@Override
	public String toString() {
		return "Hostel [name=" + name + ", address=" + address + ", contact="
				+ contact + ", restrictions=" + restrictions
				+ ", availability=" + availability + "]";
	}

}
