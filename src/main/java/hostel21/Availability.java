package hostel21;

import java.io.Serializable;

public class Availability implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4815164682623775919L;
	private String date;
	private int room;
	private int bed;
	private int price;
	private int bookedBy;
	private Hostel parent;
	

	public Hostel getParent() {
		return parent;
	}

	public void setParent(Hostel parent) {
		this.bookedBy=-1;
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bed;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + price;
		result = prime * result + room;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Availability other = (Availability) obj;
		if (bed != other.bed)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (price != other.price)
			return false;
		if (room != other.room)
			return false;
		return true;
	}

	public int getBed() {
		return bed;
	}

	public String getDate() {
		return date;
	}

	public int getPrice() {
		return price;
	}

	public int getRoom() {
		return room;
	}

	public void setBed(int bed) {
		this.bed = bed;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setRoom(int room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "Availability [date=" + date + ", room=" + room + ", bed=" + bed
				+ ", price=" + price + "]";
	}

	public int getBookedBy() {
		return bookedBy;
	}

	public void setBookedBy(int bookedBy) {
		this.bookedBy = bookedBy;
	}
}
