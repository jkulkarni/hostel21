package hostel21;

import java.io.Serializable;
import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("h21")
public class Root implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2299751812613589521L;
	@XStreamImplicit(itemFieldName = "hostel")
	ArrayList<Hostel> hostel;

	public ArrayList<Hostel> getHostel() {
		return hostel;
	}

	public void setHostel(ArrayList<Hostel> hostel) {
		this.hostel = hostel;
	}

	@Override
	public String toString() {
		return "Root [hostel=" + hostel + "]";
	}
}
