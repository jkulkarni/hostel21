package hostel21;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multimap;

public class AvailabilityFunctions {
	public static ArrayList<Availability> availabilitiesBetweenDate(
			ArrayList<Availability> avails, final String start, final String end) {
		Collection<Availability> avail = Collections2.filter(avails,
				new Predicate<Availability>() {
					public boolean apply(Availability a) {
						try {
							return a.getBookedBy() == -1 && DateFunctions.inRange(a.getDate(), start,
									end);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return false;
					}
				});
		ArrayList<Availability> a = new ArrayList<Availability>(avail);
		return a;

	}

	public static <K extends Comparable, V extends Comparable> Map<K, V> sortByValues(
			Map<K, V> map) {
		List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(
				map.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});

		// LinkedHashMap will keep the keys in the order they are inserted
		// which is currently sorted on natural ordering
		Map<K, V> sortedMap = new LinkedHashMap<K, V>();

		for (Map.Entry<K, V> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public static ArrayList<Query> generateQuery(int beds,
			ArrayList<Availability> avails) {

		Multimap<BedRoomLink, Availability> bedroomlinks = ArrayListMultimap
				.create();

		for (Availability a : avails) {
			BedRoomLink bedRoomLink = new BedRoomLink(a.getRoom(), a.getBed());
			bedroomlinks.put(bedRoomLink, a);
		}
		//System.out.println(bedroomlinks.toString());
		// calculate cheapest
		Map<BedRoomLink, Integer> pricemap = new HashMap<BedRoomLink, Integer>();
		int price = 0;
		for (BedRoomLink bdl : bedroomlinks.keySet()) {

			for (Availability a : bedroomlinks.get(bdl)) {
				price += a.getPrice();
			}
			pricemap.put(bdl, price);
			price=0;
		}
		Map<BedRoomLink, Integer> sorted = sortByValues(pricemap);
		ArrayList<Query> queries = new ArrayList<Query>();
		int count = 0;
		boolean start = false;
		ArrayList<Availability> availArrList = new ArrayList<Availability>();
		ArrayList<BedRoomLink> keys = new ArrayList<BedRoomLink>();

		for(BedRoomLink bdl: sorted.keySet()){
			keys.add(bdl);
		}
		for(int i = 0;i<beds;i++){
			if(i>=keys.size()) return queries;
			availArrList.addAll(bedroomlinks.get(keys.get(i)));
		}
		queries.add(new Query(availArrList));
		return queries;
	}

	public static Map<Integer, Collection<Availability>> getRoomMap(
			ArrayList<Availability> availability) {

		Map<Integer, Collection<Availability>> roomMap = new HashMap<Integer, Collection<Availability>>();
		HashSet<Integer> roomHash = new HashSet<Integer>();
		ArrayList<Integer> rooms = new ArrayList<Integer>();

		for (Availability a : availability) {
			roomHash.add(a.getRoom());
		}
		rooms.addAll(roomHash);

		for (final Integer room : rooms) {
			Collection<Availability> temp = Collections2.filter(availability,
					new Predicate<Availability>() {
						public boolean apply(Availability a) {
							return a.getRoom() == room;
						}
					});

			roomMap.put(room, temp);
		}
		return roomMap;
	}

	public static Multimap<String, Availability> enoughBeds(Hostel hostel,
			int beds, String start, String end) throws ParseException {
		Multimap<String, Availability> dates = ArrayListMultimap.create();

		for (Availability a : hostel.getAvailability()) {
			if (DateFunctions.inRange(a.getDate(), start, end)) {
				dates.put(a.getDate(), a);
			}
			for (String date : dates.keySet()) {
				if (dates.get(date).size() < beds) {
					System.err.println("Not enough beds on " + date);
					return null;
				}
			}
		}
		return dates;

	}

	public static String createQuery(Multimap<String, Availability> dates) {
		for (String date : dates.keySet()) {
			getRoomMap((ArrayList<Availability>) dates.get(date));
		}
		return null;
	}

	// public static boolean createQuery(Session sess,
	// Multimap<Integer,Availability> roomMap, String start, String end, int
	// beds) throws ParseException{
	// Multimap<Integer,Availability> query = ArrayListMultimap.create();
	// Multimap<Integer, Integer> pricemap = ArrayListMultimap.create();
	//
	//
	// for(Integer i : roomMap.keySet()){
	// ArrayList<Availability> availabilitiesPerRoom = new
	// ArrayList<Availability>();
	// for(Availability a : roomMap.get(i)){
	//
	// if(DateFunctions.inRange(a.getDate(), start, end)){
	// availabilitiesPerRoom.add(a);
	// }
	//
	// }
	// boolean hasStart = false, hasEnd = false;
	// for(Availability inRoom : availabilitiesPerRoom){
	// if(!hasStart)
	// hasStart = (DateFunctions.toEpoch(inRoom.getDate()) ==
	// DateFunctions.toEpoch(start));
	// if(!hasEnd)
	// hasEnd = (DateFunctions.toEpoch(inRoom.getDate()) ==
	// DateFunctions.toEpoch(start));
	//
	// }
	// if(!hasStart || !hasEnd){
	// return false;
	// }
	// for(Availability a: availabilitiesPerRoom){
	// query.put(a.getBed(), a);
	// pricemap.put(a.getBed(),a.getPrice());
	// }
	//
	// //go through each bed and count if there are enough dates
	// for(Integer bed : query.keySet()){
	// int between = DateFunctions.getDaysBetween(start, end);
	// if(query.get(bed).size() != between){
	// System.err.println("Availability list does not contain enough dates to the query.");
	// return false;
	// }
	// }
	// //create loop that adds availabilities of the bed through the dates until
	// the maximum beds is reached
	// for()
	//
	// }
	// }

	public static Multimap<Integer, Availability> getRooms(
			ArrayList<Availability> alist) {
		Multimap<Integer, Availability> roommap = ArrayListMultimap.create();

		for (Availability availability : alist) {
			if (availability.getBookedBy() != -1) {
				roommap.put(availability.getRoom(), availability);
			}
		}
		return roommap;

	}

	public static boolean searchAvail(int beds, ArrayList<Availability> alist) {
		HashMultiset<String> dates = HashMultiset.create();
		HashMultiset<String> booked = HashMultiset.create();
		Multimap<String, Integer> mm = ArrayListMultimap.create();

		for (Availability a : alist) {
			dates.add(a.getDate());
			mm.put(a.getDate(), a.getPrice());
			if (a.getBookedBy() != -1) {
				booked.add(a.getDate());
			}
		}

		for (String s : mm.keySet()) {
			if ((dates.count(s) - booked.count(s)) < beds) {
				System.err.println("Not Enough beds on " + s);
				return false;
			}
		}
		return true;
	}

	public static void searchAvailWithDate(ArrayList<Availability> alist) {
		HashMultiset<String> dates = HashMultiset.create();
		HashMultiset<String> booked = HashMultiset.create();
		Multimap<String, Integer> mm = ArrayListMultimap.create();

		for (Availability a : alist) {
			dates.add(a.getDate());
			mm.put(a.getDate(), a.getPrice());
			if (a.getBookedBy() != -1) {
				booked.add(a.getDate());
			}
		}

		for (String s : mm.keySet()) {
			System.out.println(s + ": " + (dates.count(s) - booked.count(s))
					+ " beds available between $" + Collections.min(mm.get(s))
					+ " and $" + Collections.max(mm.get(s)));
		}
	}

	public static ArrayList<Query> searchToQuery(int beds,
			HashMap<Hostel, ArrayList<Availability>> hm) {
		HashMultiset<String> dates = HashMultiset.create();
		HashMultiset<String> booked = HashMultiset.create();

		boolean allAvailable = true;

		for (Entry<Hostel, ArrayList<Availability>> e : hm.entrySet()) {
			Hostel h = e.getKey();
			ArrayList<Availability> alist = e.getValue();
			for (Availability a : alist) {
				dates.add(a.getDate());
				if (a.getBookedBy() != -1) {
					booked.add(a.getDate());
				}
			}

			for (String s : dates.elementSet()) {
				if (beds > dates.count(s) - booked.count(s)) {
					allAvailable = false;
					System.out.println("Sorry not enough beds available");
				}
			}
			if (allAvailable) {

			}

		}
		return null;
	}
}
