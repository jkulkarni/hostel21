package hostel21;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.thoughtworks.xstream.XStream;

public class XmlFunctions {
	public static Root importXml(String filepath) {
		Root root = null;

		try {
			FileReader fileReader = new FileReader(filepath); // load our xml
																// file
			XStream xstream = new XStream(); // init XStream
			// define root alias so XStream knows which element and which class
			// are
			// equivalent
			xstream.alias("h21", Root.class);
			xstream.alias("hostel", Hostel.class);
			xstream.alias("address", Address.class);
			xstream.alias("contact", Contact.class);
			xstream.alias("restriction", Restriction.class);
			xstream.alias("availability", Availability.class);

			xstream.autodetectAnnotations(true);
			xstream.processAnnotations(Root.class);
			xstream.processAnnotations(Hostel.class);
			xstream.processAnnotations(Address.class);
			xstream.processAnnotations(Contact.class);
			xstream.processAnnotations(Restriction.class);
			xstream.processAnnotations(Availability.class);
			root = (Root) xstream.fromXML(fileReader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i = 0; i < root.getHostel().size(); i++){
			for(int j = 0;j<root.getHostel().get(i).getAvailability().size();j++){
				root.getHostel().get(i).getAvailability().get(j).setParent(root.getHostel().get(i));
			}
		}
		return root;
	}

	public static Session importSession() {
		Session session = null;
		String filepath = "h21.ser";
		try {
			FileInputStream fileIn = new FileInputStream(filepath);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			session = (Session) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			System.out.println("Session class not found");
			c.printStackTrace();
		}
		return session;

	}

	public static void exportSession(Session s) {
		String filepath = "h21.ser";
		try {
			FileOutputStream fileOut = new FileOutputStream(filepath);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(s);
			out.close();
			fileOut.close();
			//System.out.printf("Serialized data is saved in " + filepath);
		} catch (IOException i) {
			i.printStackTrace();
		}

	}

}
