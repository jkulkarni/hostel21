package hostel21;

import java.io.Serializable;
import java.util.ArrayList;

public class Session implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6749827107382766962L;
	private Root root;
	private ArrayList<User> users;
	private ArrayList<Query> queries;
	private ArrayList<Query> bookings;

	public Session() {
		root = new Root();
		queries = new ArrayList<Query>();
		users = new ArrayList<User>();
		bookings = new ArrayList<Query>();
	}

	public Session(Root root, ArrayList<Query> queries) {
		super();
		this.root = root;
		this.queries = queries;
	}

	public String processQuery(int index, int user) {
		if (index >= this.queries.size()) {
			return "ID:" + index + " does not exist!";
		}
		Query q = queries.remove(index);

		for (Availability a : q.getBooking()) {
			int hostelID = root.getHostel().indexOf(a.getParent());
			int availabilityID = root.getHostel().get(hostelID)
					.getAvailability().indexOf(a);
			root.getHostel().get(hostelID).getAvailability()
					.get(availabilityID).setBookedBy(user);
		}

		idQueries();
		int bookingID = addBooking(q);
		String output = "Booking successful! Here's the detail of your booking:\n Hostel Name:"
				+ q.getBooking().get(0).getParent().getInfo()
				+ "\nCheck in: "
				+ q.getStart()
				+ "\nCheck out:"
				+ q.getEnd()
				+ "\nBeds: "
				+ q.getBeds()
				+ "\nBookingID: "
				+ bookingID
				+ "\nName: "
				+ users.get(user).getFirst() +" "+users.get(user).getLast()  + "\n"
						+ "Price: $" + q.getPrice();
		return output;

	}

	public boolean removeBooking(int index) {
		Query removed = bookings.remove(index);
		if(removed == null) return false;
		for (Availability a : removed.getBooking()) {
			int hostelID = root.getHostel().indexOf(a.getParent());
			int availabilityID = root.getHostel().get(hostelID)
					.getAvailability().indexOf(a);
			root.getHostel().get(hostelID).getAvailability()
					.get(availabilityID).setBookedBy(-1);
		}
		return true;

	}

	public ArrayList<Query> getBookings() {
		return bookings;
	}

	public void setBookings(ArrayList<Query> bookings) {
		this.bookings = bookings;
	}

	public int addBooking(Query query) {
		this.bookings.add(query);
		idBookings();
		return bookings.indexOf(query);

	}

	public String addQueries(ArrayList<Query> queries) {
		int size = this.queries.size();
		this.queries.addAll(queries);
		idQueries();
		String output = "";
		for (Query q : queries) {
			q.setLocalID(size++);
			output += q.searchString() + "\n";
		}
		return output;
	}

	private void idBookings() {
		for (int i = 0; i < this.bookings.size(); i++) {
			this.bookings.get(i).setLocalID(i);
		}
	}

	private void idQueries() {
		for (int i = 0; i < this.queries.size(); i++) {
			this.queries.get(i).setLocalID(i);
		}
	}

	public ArrayList<Query> getQueries() {
		return queries;
	}

	public Root getRoot() {
		return root;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public void setQueries(ArrayList<Query> queries) {
		this.queries = queries;
	}

	public void setRoot(Root root) {
		this.root = root;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

}
