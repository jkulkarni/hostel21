package hostel21;

import java.io.File;
import java.io.FileNotFoundException;

import com.beust.jcommander.JCommander;

import commands.CommandAdmin;
import commands.CommandBook;
import commands.CommandMain;
import commands.CommandSearch;
import commands.CommandUser;

public class Application {

	private String args[];
	private CommandAdmin ca;

	private CommandMain cm;

	private CommandSearch cs;
	private CommandUser cu;
	private CommandBook cb;

	JCommander jc;

	private Session session;

	public Application(String[] args) {
		this.args = args;

		session = new Session();

		cm = new CommandMain();
		jc = new JCommander(cm);
		cs = new CommandSearch();
		ca = new CommandAdmin();
		cu = new CommandUser();
		cb = new CommandBook();
		jc.addCommand("search", cs);
		jc.addCommand("admin", ca);
		jc.addCommand("user",cu);
		jc.addCommand("book",cb);
		jc.parse(args);
	}

	public String[] getArgs() {
		return args;
	}

	public CommandAdmin getCa() {
		return ca;
	}

	public CommandMain getCm() {
		return cm;
	}

	public CommandSearch getCs() {
		return cs;
	}

	public JCommander getJc() {
		return jc;
	}

	public Session getSession() {
		return session;
	}

	public void importSession() {
		File file = new File("h21.ser");
		if (file.exists()) {
			session = XmlFunctions.importSession();
		}

	}

	public void loadXml() throws FileNotFoundException {

		session.setRoot(ca.loadXml());
		

	}
	
	public void exportSession(){
		XmlFunctions.exportSession(session);
	}

	public void run() {
		this.importSession();
		try {
			if (args.length > 0 && !args[0].equals("")) {
				if (jc.getParsedCommand().equals("admin")) {
					if (!ca.run(session))
						this.loadXml();
				}
				if (jc.getParsedCommand().equals("search")) {
					cs.run(session);
				}
				if (jc.getParsedCommand().equals("user")) {
					cu.run(session);
				}
				if (jc.getParsedCommand().equals("book")){
					cb.run(session);
				}
				
				this.exportSession();


			} else
				jc.usage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			jc.usage();
			e.printStackTrace();
		}

	}
}
