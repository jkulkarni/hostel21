package hostel21;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("contact")
public class Contact implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1805740048618288716L;
	private String phone;
	private String email;

	private String facebook;

	private String web;

	public String getEmail() {
		return email;
	}

	public String getFacebook() {
		return facebook;
	}

	public String getPhone() {
		return phone;
	}

	public String getWeb() {
		return web;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	@Override
	public String toString() {
		return "Contact [phone=" + phone + ", email=" + email + ", facebook="
				+ facebook + ", web=" + web + "]";
	}

}
