package hostel21;

public class DateAvailMap {
	private int bed;
	private Availability avail;
	public int getBed() {
		return bed;
	}
	public void setBed(int bed) {
		this.bed = bed;
	}
	public Availability getAvail() {
		return avail;
	}
	public void setAvail(Availability avail) {
		this.avail = avail;
	}
	public DateAvailMap(int bed, Availability avail) {
		super();
		this.bed = bed;
		this.avail = avail;
	}
	

}
