package hostel21;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("address")
public class Address  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2383905790839584574L;
	private String street;
	private String city;
	private String state;

	@XStreamAlias("postal_code")
	private String postalcode;

	private String country;

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public String getState() {
		return state;
	}

	public String getStreet() {
		return street;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", city=" + city + ", state="
				+ state + ", postalcode=" + postalcode + ", country=" + country
				+ "]";
	}

}
