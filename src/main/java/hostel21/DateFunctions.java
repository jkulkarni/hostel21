package hostel21;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList; 
import java.util.Date;

public class DateFunctions {
	public static String toDate(long epoch){
		Date d = new Date(epoch);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(d);
		
	}
	
	public static long toEpoch(String date) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.parse(date).getTime();
	}
	
	public static boolean inRange(String in,String start,String end) throws ParseException{
		if(toEpoch(start)<=toEpoch(in) && toEpoch(in)<toEpoch(end) )
			return true;
		return false;
		
	}
	public static int getDaysBetween(String start, String end) throws ParseException{
		return (int) ((toEpoch(end)-toEpoch(start))/86400000);
		
		
	}
	public static boolean enoughBedsWithinDates(ArrayList<Availability> beds, String start, String end){
		return false;
		
	}

}
