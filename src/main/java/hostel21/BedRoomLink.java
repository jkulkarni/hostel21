package hostel21;


public class BedRoomLink implements Comparable<BedRoomLink>{
	private int room;
	private int bed;
	
	public int getRoom() {
		return room;
	}
	public void setRoom(int room) {
		this.room = room;
	}
	public int getBed() {
		return bed;
	}
	public void setBed(int bed) {
		this.bed = bed;
	}
	public BedRoomLink(int room, int bed) {
		super();
		this.room = room;
		this.bed = bed;
	}
	public String getID(){
		return ""+room+""+bed;
	}
	
	  public int compareTo(BedRoomLink that) {
		  return this.getID().compareTo(that.getID());
	  }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bed;
		result = prime * result + room;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BedRoomLink other = (BedRoomLink) obj;
		if (bed != other.bed)
			return false;
		if (room != other.room)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "BedRoomLink [room=" + room + ", bed=" + bed + "]";
	}


}
