package hostel21;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;

public class Query implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3708145093055979111L;
	private int localID;
	private int price;
	private int beds;
	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}

	private int user;
	private String rooms;
	private String start;
	private String end;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getBeds() {
		return beds;
	}

	public void setBeds(int beds) {
		this.beds = beds;
	}

	public String getRooms() {
		return rooms;
	}

	public void setRooms(String rooms) {
		this.rooms = rooms;
	}

	private ArrayList<Availability> booking;

	public Query(ArrayList<Availability> booking) {
		this.booking = booking;
		this.user =-1;
		this.price=0;
		HashSet<Integer> rooms = new HashSet<Integer>();
		HashSet<Integer> beds = new HashSet<Integer>();
		HashSet<String> dates = new HashSet<String>();

		for (Availability a : this.booking) {
			beds.add(a.getBed());
			rooms.add(a.getRoom());
			dates.add(a.getDate());
			price+=a.getPrice();
		}
		HashMap<Long, String> datemap = new HashMap<Long, String>();
		for (String date : dates) {
			try {
				datemap.put(DateFunctions.toEpoch(date), date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Map<Long, String> sortedDates = Maps.newTreeMap(Ordering.natural());
		sortedDates.putAll(datemap);
		ArrayList<Long> ldateList = new ArrayList<Long>();
		for(Long d : sortedDates.keySet()){
			ldateList.add(d);
		}
		
		Long startIndex = ldateList.get(0);
		Long endIndex = ldateList.get(sortedDates.size() - 1);

		this.start = sortedDates.get(startIndex);
		this.end = DateFunctions.toDate(endIndex+86400000);

		this.beds = beds.size();
		this.rooms = "";
		for (Integer room : rooms) {
			this.rooms += "#" + room + ",";
		}

	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public int getLocalID() {
		return localID;
	}

	public void setLocalID(int localID) {
		this.localID = localID;
	}

	public ArrayList<Availability> getBooking() {
		return booking;
	}

	public void setBooking(ArrayList<Availability> booking) {
		this.booking = booking;
	}
	public String searchString(){
		return "search_id:"+this.localID+", $"+this.price+", rooms "+this.rooms;
		
	}

	@Override
	public String toString() {
		return "Booking [localID=" + localID + ", price=" + price + ", beds="
				+ beds + ", rooms=" + rooms + ", start=" + start + ", end="
				+ end + ", booking=" + booking + "]";
	}


}
