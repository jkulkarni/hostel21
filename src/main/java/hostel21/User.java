package hostel21;

import java.io.Serializable;

public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2327639112344084549L;
	private int myId;
	public User(String first, String last, String email) {
		super();
		this.first = first;
		this.last = last;
		this.email = email;
	}
	@Override
	public String toString() {
		return "User [myId=" + myId + ", first=" + first + ", last=" + last
				+ ", email=" + email + ", cc=" + cc + ", expiration="
				+ expiration + ", security=" + security + ", phone=" + phone
				+ "]";
	}
	public int getMyId() {
		return myId;
	}
	public void setMyId(int myId) {
		this.myId = myId;
	}
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	public String getLast() {
		return last;
	}
	public void setLast(String last) {
		this.last = last;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	private String first;
	private String last;
	private String email;
	private String cc;
	private String expiration;
	private String security;
	private String phone;
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getExpiration() {
		return expiration;
	}
	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}
	public String getSecurity() {
		return security;
	}
	public void setSecurity(String security) {
		this.security = security;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
