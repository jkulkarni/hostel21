package hostel21;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class Restriction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6390149896375974545L;
	@XStreamAlias("check_in_time")
	private String checkin;
	@XStreamAlias("check_out_time")
	private String checkout;
	private String smoking;
	private String alcohol;
	@XStreamAlias("cancellation_deadline")
	private String deadline;
	@XStreamAlias("cancellation_penalty")
	private String penalty;

	public String getAlcohol() {
		return alcohol;
	}

	public String getCheckin() {
		return checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public String getSmoking() {
		return smoking;
	}

	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

}
