package commands;

import java.io.FileNotFoundException;

import hostel21.Hostel;
import hostel21.Query;
import hostel21.Root;
import hostel21.Session;
import hostel21.XmlFunctions;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
@Parameters(separators = " ", commandDescription = "Search hostels")
public class CommandAdmin {
	@Parameter(names = "-load", description = "load xml file")
	  private String load ="";
	
	@Parameter(names = "-revenue", description = "show revenue")
	  private boolean revenue=false;
	
	@Parameter(names = "-occupancy", description = "show")
	  private boolean occupancy=false;
	
	public Root loadXml() throws FileNotFoundException{
		if(load==null){
			throw new FileNotFoundException();
		}
		System.out.println(load+" successfully loaded!");
		return XmlFunctions.importXml(load);
	}
	
	public boolean run(Session s){
		if(!occupancy&&!revenue)
			return false;
		if(occupancy){
			runOccupancy(s);
		}
		if(revenue){
			runRevenue(s);
		}
		
		return true;
	}

	private void runRevenue(Session s) {
		int revenue = 0;
		for(Query q :s.getBookings()){
			revenue += q.getPrice();
		}
		System.out.println("Total Revenue: $"+revenue);
	}

	private void runOccupancy(Session s) {
		int total = 0;
		for(Hostel h : s.getRoot().getHostel()){
			total += h.getAvailability().size();
		}
		int reserved = 0;
		for(Query q :s.getBookings()){
			reserved += q.getBooking().size();
		}
		System.out.print(reserved+" availabilities booked out of "+total+" total\n");
	}


}
