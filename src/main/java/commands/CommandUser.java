package commands;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import hostel21.Session;
import hostel21.User;

@Parameters(separators = "=", commandDescription = "user function")
public class CommandUser {
	@Parameter(names = "-add", description = "add a user")
	private boolean add;
	@Parameter(names = "-change", description = "change a user by id")
	private boolean change;
	@Parameter(names = "-view", description = "view a user by id")
	private boolean view;

	@Parameter(names = "--user_id", description = "user id", required = false)
	private String id;
	@Parameter(names = "--first_name", description = "First Name", required = false)
	private String first;
	@Parameter(names = "--last_name", description = "Last Name", required = false)
	private String last;
	@Parameter(names = "--email", description = "email address", required = false)
	private String email;

	public void run(Session s) {
		if (add) {

			if (first != null && last != null && email != null) {
				for(User u : s.getUsers()){
					if(u.getEmail().equals(email)){
						System.err.println("Email already exists, please use a different email");
						return;
					}
				}
				s.getUsers().add(new User(first, last, email));
				for(int i = 0; i< s.getUsers().size();i++){
					s.getUsers().get(i).setMyId(i);
				}
				System.out.println("added new user");
			} else
				System.err
						.println("First, last, and email required to add user");

		} else if (change) {
			if (id == null) {
				System.err.println("id required");
			} else {
				for (User u : s.getUsers()) {
					if (u.getMyId() == Integer.parseInt(id)) {
						if (first != null)
							u.setFirst(first);
						if (last != null)
							u.setLast(last);
						if (email != null)
							u.setEmail(email);
						System.out.println("User changed! "+u);
					}
				}
			}

		} else if (view) {
			if (id == null) {
				for (User u : s.getUsers()) {
					System.out.println(u.toString());
				}
			} else {
				for (User u : s.getUsers()) {
					if (u.getMyId() == Integer.parseInt(id))
						System.out.println(u.toString());
				}
			}

		}

	}

}
