package commands;


import hostel21.Session;

import com.beust.jcommander.Parameter;

public class CommandBook {
	@Parameter(names = "-add", description = "add a booking")
	private boolean add;
	@Parameter(names = "-cancel", description = "cancel a booking id")
	private boolean cancel;
	@Parameter(names = "-view", description = "view a booking by id")
	private boolean view;

	@Parameter(names = "--user_id", description = "user id who will make to booking", required = false)
	private String userid;
	@Parameter(names = "--search_id", description = "search id of the booking", required = false)
	private String searchid;
	@Parameter(names = "--booking_id", description = "id of the booking", required = false)
	private String bookingid;

	public void run(Session session){
		if(add){
			if(this.userid==null && this.searchid == null){
				System.out.println("Please supply both user and searchid");
				return;
			} else {
				System.out.println(session.processQuery(Integer.parseInt(searchid),Integer.parseInt(userid)));
			}
		} else if(cancel){
			if(bookingid == null){
				System.out.println("Please supply a booking ID");
				return;
			} else {
				if(session.removeBooking(Integer.parseInt(bookingid))){
					System.out.println("Sucessfully removed");
				} else System.out.println("Check if booking ID exists");
			}
		} else if(view){
			if(bookingid!=null){
				System.out.println(session.getQueries().get(Integer.parseInt(bookingid)).toString());
			}
			else{
				for(hostel21.Query q : session.getBookings()){
					System.out.println(q.toString());
				}
				if(session.getBookings().size()==0) System.out.println("No bookings booked");
			}
		}
	}

}
