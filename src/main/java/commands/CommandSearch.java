package commands;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hostel21.Availability;
import hostel21.AvailabilityFunctions;
import hostel21.DateFunctions;
import hostel21.Hostel;
import hostel21.Query;
import hostel21.Session;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=", commandDescription = "Search hostels")
public class CommandSearch {
	@Override
	public String toString() {
		return "CommandSearch [city=" + city + ", startdate=" + startdate
				+ ", enddate=" + enddate + ", beds=" + beds + "]";
	}

	@Parameter(names = "--city", description = "City", required = false)
	private String city;
	@Parameter(names = "--start_date", description = "Start date", required = false)
	private String startdate;
	@Parameter(names = "--end_date", description = "End date", required = false)
	private String enddate;
	@Parameter(names = "--beds", description = "amount of beds", required = false)
	private int beds;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public int getBeds() {
		return beds;
	}

	public void setBeds(int beds) {
		this.beds = beds;
	}

	public void dateSearch() {

	}

	public HashMap<Hostel, ArrayList<Availability>> availableList(
			ArrayList<Hostel> hostels) throws ParseException {
		HashMap<Hostel, ArrayList<Availability>> map = new HashMap<Hostel, ArrayList<Availability>>();
		for (Hostel h : hostels) {
			ArrayList<Availability> avail = new ArrayList<Availability>();
			for (Availability a : h.getAvailability()) {
				if (DateFunctions.inRange(a.getDate(), startdate, enddate)) {
					avail.add(a);
				}
				map.put(h, avail);
			}
		}
		return map;
	}

	public ArrayList<Hostel> hostelsOfCity(Session session) {
		ArrayList<Hostel> result = new ArrayList<Hostel>();
		for (Hostel h : session.getRoot().getHostel()) {
			if (h.getAddress().getCity().equals(this.city)) {
				result.add(h);
			}
		}
		if (result.size() < 1)
			System.out.println("Nothing Found for " + this.city);
		return result;
	}

	public void run(Session session) throws ParseException {
		if (this.getBeds() == 0 && this.getCity() == null
				&& this.getEnddate() == null && this.getStartdate() == null)
			searchAll(session);
		// System.out.println(this.toString());
		if (this.city != null && this.startdate != null && this.enddate != null
				&& this.beds == 0) {
			for (Entry<Hostel, ArrayList<Availability>> e : availableList(
					hostelsOfCity(session)).entrySet()) {
				Hostel h = e.getKey();
				System.out.println(h.getName() + ", "
						+ h.getAddress().getCity());
				AvailabilityFunctions.searchAvailWithDate(e.getValue());
			}

		} else if (this.getBeds() > 0 && this.getCity() != null
				&& this.getEnddate() != null && this.getStartdate() != null) {
			Map<Hostel, ArrayList<Availability>> hostelavailmap = new HashMap<Hostel, ArrayList<Availability>>();

			for (Hostel hostel : session.getRoot().getHostel()) {
				hostelavailmap.put(hostel, AvailabilityFunctions
						.availabilitiesBetweenDate(hostel.getAvailability(),
								this.getStartdate(), this.getEnddate()));
			}
			ArrayList<Query> queries = new ArrayList<Query>();
			for (Hostel h : hostelavailmap.keySet()) {
				
				

				System.out.println("Hostel: "+h.getName());
				AvailabilityFunctions.searchAvail(beds,
						hostelavailmap.get(h));


				
				queries.addAll(AvailabilityFunctions.generateQuery(this.beds, hostelavailmap.get(h)));
				System.out.println(session.addQueries(queries));
				
//				List<Query> outputList;
//				if(session.getQueries().size()==queries.size()){
//					outputList = session.getQueries();
//				} else if(queries.size()==1){
//					outputList = session.getQueries().subList(session.getQueries().size()-2,session.getQueries().size()-1);
//				} else outputList = session.getQueries().subList(session.getQueries().size()-(1+queries.size()), session.getQueries().size()-1);
//				
//				
//				for(Query q : session.getQueries()){
//					System.out.println(q.searchString());
//				}
				queries.clear();
			}

			
			

		} else if (this.city != null) {
			System.out.println(this.city + " hostels");
			for (Hostel h : hostelsOfCity(session)) {
				System.out.println(h.niceString());
			}

		}
	}

	private void searchAll(Session s) {
		{
			for (Hostel h : s.getRoot().getHostel()) {
				System.out.println(h.niceString());
			}
		}
	}
}
